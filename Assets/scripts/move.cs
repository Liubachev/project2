﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class move : MonoBehaviour

{
    
    public Vector3 direction; //направление 
   
    public Rigidbody rb;
    public float speed;


    void FixedUpdate()
    {

        if (spawn.secondtouch != new Vector3(0, 0, 0))
        {
            direction = spawn.firsttouch - spawn.secondtouch; //высчитываем вектор и он собственно и равен направлению броска
            rb.AddForce(direction * speed, ForceMode.Impulse); ; 
            spawn.firsttouch = new Vector3(0, 0, 0);
            spawn.secondtouch = new Vector3(0, 0, 0);

        }
    }


    private void OnTriggerEnter(Collider other) //вызываем если попали в чей-то коллайдер
    {
        if (other.tag == "target") //если это ворота, то меняем уровень и удаляем нынешнего игрока, при респавне появится новый в начальной точке
        {
            spawn.changelevel = true;
            Destroy(this.gameObject); 
        }

        if (other.tag == "enemy") //если это красный круг, перезагружаем сцену
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}