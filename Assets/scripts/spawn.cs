﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class spawn : MonoBehaviour
{

    //спавним  количество красных кругов  равное нынешнему уровню 
    // (сейчас когда ставлю комментарии понимаю что весьма глупо следлано, как будет доступ к юнити перепишу, большую часть скрипта)

    public Camera cam;               //основная камера
    public GameObject player;        //игрок 
    public GameObject target;        //цель
    public GameObject enemy;         //красные круги
    public List<GameObject> enemies; // список кругов

    public static Vector3 firsttouch; //точка нажатия на экран
    public static Vector3 secondtouch; //точка где мы отпустили палец

    public Text leveltext; //лвл 


    public static int level; //лвл

    public static bool changelevel; //смена уровня 

    public bool onfortarget; // bool для спавна ворот


    public bool onforenemy; //bool для спавна кругов

    void Awake()
    {
        onfortarget = true;
        level = 0; // изначально уровень нулевой для ознакомления со свайпами
       
    }



    private void FixedUpdate()
    {
        leveltext.text = level.ToString();


        if (changelevel)
        {
            level += 1;
            onfortarget = true;
            onforenemy = true;
            changelevel = false;
            enemies.Clear();
        }



        if (Input.GetMouseButtonDown(0)) //опустили палец
        {firsttouch = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));}

        if (Input.GetMouseButtonUp(0)) //подняли палец
        {secondtouch = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));}

        Spawnenemy(); // спавним красные круги 


        Spawntargetandplayer(); //спавним ворота и переспавниваем игрока
    }




    void Spawntargetandplayer()
    {

        if (onfortarget)
        {
            Instantiate(player);
            Instantiate(target);
            float random0 = Random.Range(-2, 2);
            target.transform.position = transform.position + new Vector3(random0, target.transform.position.y, 0);
            onfortarget = false;
        }


    }




    void Spawnenemy()
    {
        if (onforenemy)
        {
            if (enemies.Count < level) //спавним  количество красных кругов равное нынешнему уровню в случайном месте
            {
                GameObject newenemy = Instantiate(enemy);
                float random = Random.Range(-2f, 2f);
                float random1 = Random.Range(2f, -2f);
                newenemy.transform.position = transform.position + new Vector3(random, random1, 0);
                enemies.Add(newenemy);
            }
        }
    }



}
