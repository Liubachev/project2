﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void move::Update()
extern void move_Update_mDC8570557B216A7A5C84E9770ABB5155D7B76903 ();
// 0x00000002 System.Void move::.ctor()
extern void move__ctor_m235BF92B282E0A732878971FA6A28D6EA9EDD70B ();
// 0x00000003 System.Void spawn::Awake()
extern void spawn_Awake_mA0852C69A4DDCDF7B758EC154F0981986BA702B3 ();
// 0x00000004 System.Void spawn::Update()
extern void spawn_Update_mA2CD215DEF560A9CB524152BE59B26EC4AC4236B ();
// 0x00000005 System.Void spawn::.ctor()
extern void spawn__ctor_m71EC38F208D9D34314FC2D55A5DD2DE83D5A93E9 ();
static Il2CppMethodPointer s_methodPointers[5] = 
{
	move_Update_mDC8570557B216A7A5C84E9770ABB5155D7B76903,
	move__ctor_m235BF92B282E0A732878971FA6A28D6EA9EDD70B,
	spawn_Awake_mA0852C69A4DDCDF7B758EC154F0981986BA702B3,
	spawn_Update_mA2CD215DEF560A9CB524152BE59B26EC4AC4236B,
	spawn__ctor_m71EC38F208D9D34314FC2D55A5DD2DE83D5A93E9,
};
static const int32_t s_InvokerIndices[5] = 
{
	23,
	23,
	23,
	23,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	5,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
